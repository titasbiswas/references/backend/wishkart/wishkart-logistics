package com.wishkart.logistics.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.sql.Timestamp;


/**
 * The persistent class for the outbound_shipping database table.
 * 
 */
@Entity
@Table(name="outbound_shipping")
@NamedQuery(name="OutboundShipping.findAll", query="SELECT o FROM OutboundShipping o")
@Data
@Builder
@AllArgsConstructor
@ToString
public class OutboundShipping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="bill_addr_id")
	private Long billAddrId;

	@Column(name="delivered_on")
	private Timestamp deliveredOn;

	@Column(name="delivery_comment")
	private String deliveryComment;

	@Column(name="delivery_status")
	private String deliveryStatus;

	@Column(name="item_id")
	private Long itemId;

	@Column(name="order_id")
	private Long orderId;

	private Integer qty;

	@Column(name="ship_addr_id")
	private Long shipAddrId;

	@Column(name="shipped_on")
	private Timestamp shippedOn;

	@Column(name="tracking_id")
	private Integer trackingId;
	
	@Column(name="product_id")
	private String productId;

	public OutboundShipping() {
	}

}