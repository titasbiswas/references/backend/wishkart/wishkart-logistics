package com.wishkart.logistics.rest.client;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.wishkart.logistics.model.external.OrderItem;


@FeignClient(name="WishkartSales")
@RibbonClient(name="WishkartSales")
public interface SalesProxy {

	@PutMapping(value="/updatedeliverystat", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	void updateDeliveryStatus(@RequestBody OrderItem item);
}
