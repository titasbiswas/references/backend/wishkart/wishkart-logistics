package com.wishkart.logistics.service;

import com.wishkart.logistics.model.OutboundShipping;

public interface DispatchService {

	void updateDispatchStatus(OutboundShipping outboundShipping);
}
